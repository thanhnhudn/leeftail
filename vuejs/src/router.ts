import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './views/home.vue';
import ProductList from './views/product-list.vue';
import UserList from './views/user-list.vue';

const routes = [
  { name: 'home', path: '/', component: Home, display: 'Home'},
  { name: 'products', path: '/products', component: ProductList, display: 'Products' },
  { name: 'users', path: '/users', component: UserList, display: 'Users' },
];

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;

