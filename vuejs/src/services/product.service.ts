import Product from '@/models/product';
import { apiHandler } from '@/services/api-handler';
import { helper } from '@/helpers/helper';

const header = { 'Content-Type': 'application/json' };
const api = `${helper.apiConstant}/product`;

class ProductApi {
  public async getAll() {
    return await fetch(`${api}`, {
      method: 'get',
      headers: header,
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }

  public async get(id: string) {
    return await fetch(`${api}/${id}`, {
      method: 'get',
      headers: header,
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }

  public async create(item: Product) {
    const data = JSON.stringify(item);
    return await fetch(`${api}`, {
      method: 'post',
      headers: header,
      body: data,
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }

  public async update(item: Product) {
    const data = JSON.stringify(item);
    return await fetch(`${api}/${item.id}`, {
      method: 'put',
      headers: header,
      body: data.replace('id', ''),
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }

  public async delete(id: string) {
    return await fetch(`${api}/${id}`, {
      method: 'delete',
      headers: header,
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }
}

export const productApi = new ProductApi();
