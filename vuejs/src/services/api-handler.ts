class ApiHandler {
  public handleResponse(response: any) {
    return new Promise((resolve, reject) => {
      if (response.ok) {
        // return json if it was returned in the response
        const contentType = response.headers.get('content-type');
        if (contentType && contentType.includes('application/json')) {
          response.json().then((json: string) => resolve(json));
        } else {
          resolve();
        }
      } else {
        switch (response.status) {
          case 403:
            return reject('Forbidden to action');
          default:
            // return error message from response body
            response.text().then((text: string) => reject(text));
            break;
        }
      }
    });
  }

  public handleError(error: any) {
    return Promise.reject(error && error.message);
  }
}

export const apiHandler = new ApiHandler();
