import User from '@/models/User';
import Role from '@/models/Role';
import { apiHandler } from '@/services/api-handler';
import { helper } from '@/helpers/helper';

const header = { 'Content-Type': 'application/json' };
const api = `${helper.apiConstant}/user`;

class UserApi {
  public async authenticate(user: User) {
    const data = JSON.stringify(user);
    return await fetch(`${api}/authenticate`, {
      method: 'post',
      headers: header,
      body: data,
    })
      .then(apiHandler.handleResponse, apiHandler.handleError)
      // tslint:disable-next-line:no-shadowed-variable
      .then((user: any) => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('user', JSON.stringify(user));
        }

        return user;
      });
  }

  public async register(user: User) {
    user.roles = [{ id: '', name: 'User', description: 'user' }];
    const data = JSON.stringify(user);
    return await fetch(`${api}/register`, {
      method: 'post',
      headers: header,
      body: data,
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }

  public async getAll() {
    return await fetch(api, {
      method: 'get',
      headers: helper.getAuthHeader(),
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }

  public async get(id: string) {
    return await fetch(`${api}/${id}`, {
      method: 'get',
      headers: helper.getAuthHeader(),
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }

  public async update(item: User) {
    const data = JSON.stringify(item);
    return await fetch(`${api}/update/${item.id}`, {
      method: 'post',
      headers: helper.getAuthHeader(),
      body: data.replace('id', ''),
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }

  public async deleteUser(id: string) {
    return await fetch(`${api}/${id}`, {
      method: 'delete',
      headers: helper.getAuthHeader(),
    }).then(apiHandler.handleResponse, apiHandler.handleError);
  }
}

export const userApi = new UserApi();
