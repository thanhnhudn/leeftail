import Role from './role';

export default class User {
  constructor(
    public id: string,
    public username: string,
    public firstName: string,
    public lastName: string,
    public password: string,
    public token: string,
    public roles: Role[],
  ) {}
}
