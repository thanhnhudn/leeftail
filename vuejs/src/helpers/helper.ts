class Helper {
    public apiConstant: string =
    process.env.VUE_APP_API_URL ?
     process.env.VUE_APP_API_URL :
     `http://localhost:5100/api`;

    public getAuthHeader(): {} {
        const storageUser = localStorage.getItem('user');
        // return authorization header with jwt token
        const user = JSON.parse(storageUser != null ? storageUser : '{}');

        if (user && user.token) {
        return {Authorization: 'Bearer ' + user.token };
        } else {
            return {};
        }
    }
}

export const helper = new Helper();
