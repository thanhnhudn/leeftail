﻿using Leeftail.Core.DomainModels;
using Leeftail.Core.Interfaces;
using Leeftail.Infrastructure.Data;

namespace Leeftail.Service.Services
{
    public class ProductService : MongoService<Product>
    {
        public ProductService(LeeftailContext context, IRepository<Product> productRepository)
            : base(context, productRepository)
        {
        }
    }
}
