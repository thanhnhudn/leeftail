﻿using Leeftail.Core.DomainModels;
using Leeftail.Core.Interfaces;
using Leeftail.Core.Models;
using Leeftail.Core.Models.Pagination;
using Leeftail.Infrastructure.Data;
using Leeftail.Service.Interfaces;
using Leeftail.Service.Services;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Leeftail.Service.Repositories
{
    public class UserService : MongoService<User>, IUserService<User>
    {
        private readonly IRepository<Role> RoleRepo;

        public UserService(LeeftailContext context, IRepository<User> userRepo, IRepository<Role> roleRepo)
            : base(context, userRepo)
        {
            RoleRepo = roleRepo;

            //var t = context.db.GetCollection<User>(typeof(User).Name).Aggregate()
            //      .Lookup<Role, User>(typeof(Role).Name, "RoleIds._id", "_id", "RoleList")
            //      .ToList<User>();
        }

        public User Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = this.Include(r => r.Username == username).FirstOrDefault();

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            // authentication successful
            return user;
        }

        public override IAggregateFluent<User> Include(Expression<Func<User, bool>> filter = null)
        {
            var roleLookup = new MongoLookup()
            {
                CollectionName = typeof(Role).Name,
                LocalField = "RoleIds._id",
                ForeignField = "_id",
                ResultField = "Roles"
            };
            return Repository.Include(filter, roleLookup);
        }

        public override Task<List<User>> GetAllAsync(Expression<Func<User, bool>> filter = null)
        {
            return this.Include(filter).ToListAsync();
        }

        public override Task<User> AddAsync(User user)
        {
            if (string.IsNullOrWhiteSpace(user.Password))
                throw new Exception("Password is required");

            bool existUser = Repository.GetAllAsync(r => r.Username == user.Username).Result.Any();
            if (existUser)
                throw new Exception("Username '" + user.Username + "' is already taken");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(user.Password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            if (user.Roles != null && user.Roles.Count > 0)
            {
                List<string> strRoles = user.Roles.Select(i => i.Name).ToList();
                var roles = RoleRepo.GetAllAsync(r => strRoles.Contains(r.Name)).Result;
                user.RoleIds = roles.Select(r => new MongoId() { Id = r.Id }).ToList();
                user.Roles = null;
            }

            return Repository.AddAsync(user);
        }

        public override Task<User> GetByIdAsync(ObjectId id)
        {
            return this.Include(r => r.Id == id).FirstOrDefaultAsync();
        }

        public override Task<ReplaceOneResult> UpdateAsync(ObjectId id, User userParam)
        {
            var user = Repository.GetByIdAsync(id).Result;

            if (user == null)
                throw new Exception("User not found");

            if (userParam.Username != user.Username)
            {
                // username has changed so check if the new username is already taken
                if (Repository.GetAllAsync(r => r.Username == userParam.Username).Result.Any())
                    throw new Exception("Username " + userParam.Username + " is already taken");
            }

            // update user properties
            user.FirstName = userParam.FirstName;
            user.LastName = userParam.LastName;
            user.Username = userParam.Username;

            // update password if it was entered
            if (!string.IsNullOrWhiteSpace(userParam.Password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(userParam.Password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            if (userParam.Roles != null && userParam.Roles.Count > 0)
            {
                List<string> strRoles = userParam.Roles.Select(i => i.Name).ToList();
                var roles = RoleRepo.GetAllAsync(r => strRoles.Contains(r.Name)).Result;
                user.RoleIds = roles.Select(r => new MongoId() { Id = r.Id }).ToList();
                user.Roles = null;
            }

            return Repository.UpdateAsync(id, user);
        }

        public override Task<PaginatedList<User>> GetPaginatedIncludeAsync(PaginationBase parameters, Expression<Func<User, bool>> filter)
        {
            var roleLookup = new MongoLookup()
            {
                CollectionName = typeof(Role).Name,
                LocalField = "RoleIds._id",
                ForeignField = "_id",
                ResultField = "Roles"
            };
            return Repository.GetPaginatedAsync(parameters, filter, roleLookup);
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }
    }
}
