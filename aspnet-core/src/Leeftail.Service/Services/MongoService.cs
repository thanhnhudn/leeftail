﻿using Leeftail.Core.Bases;
using Leeftail.Core.Interfaces;
using Leeftail.Core.Models.Pagination;
using Leeftail.Infrastructure.Data;
using Leeftail.Service.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Leeftail.Service.Services
{
    public abstract class MongoService<T> : UnitOfWork, IService<T> where T : EntityAuditBase
    {
        public readonly IRepository<T> Repository;

        public MongoService(LeeftailContext context, IRepository<T> repository)
            : base(context)
        {
            Repository = repository;
        }

        public virtual IAggregateFluent<T> Include(Expression<Func<T, bool>> filter = null)
        {
            return Repository.Include(filter, null);
        }

        public virtual Task<T> AddAsync(T model)
        {
            StartTransaction();
            var result = Repository.AddAsync(model);
            CommitTransactionAsync();
            return result;
        }

        public virtual Task<List<T>> GetAllAsync(Expression<Func<T, bool>> filter = null)
        {
            return Repository.GetAllAsync(filter);
        }

        public virtual Task<T> GetByIdAsync(ObjectId id)
        {
            return Repository.GetByIdAsync(id);
        }

        public virtual Task<ReplaceOneResult> UpdateAsync(ObjectId id, T item)
        {
            StartTransaction();
            var result = Repository.UpdateAsync(id, item);
            CommitTransactionAsync();
            return result;
        }

        public virtual Task<DeleteResult> DeleteAsync(Expression<Func<T, bool>> expression)
        {
            StartTransaction();
            var result = Repository.DeleteAsync(expression);
            CommitTransactionAsync();
            return result;
        }

        public virtual Task<DeleteResult> DeleteAllAsync()
        {
            StartTransaction();
            var result = Repository.DeleteAllAsync();
            CommitTransactionAsync();
            return result;
        }

        public virtual Task<PaginatedList<T>> GetPaginatedAsync(PaginationBase parameters)
        {
            return Repository.GetPaginatedAsync(parameters);
        }

        public virtual Task<PaginatedList<T>> GetPaginatedAsync(PaginationBase parameters, Expression<Func<T, bool>> filter)
        {
            return Repository.GetPaginatedAsync(parameters, filter);
        }

        public virtual Task<PaginatedList<T>> GetPaginatedIncludeAsync(PaginationBase parameters, Expression<Func<T, bool>> filter)
        {
            return Repository.GetPaginatedAsync(parameters, filter, null);
        }
    }
}
