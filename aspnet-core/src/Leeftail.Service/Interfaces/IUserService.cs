﻿using Leeftail.Core.DomainModels;

namespace Leeftail.Service.Interfaces
{
    public interface IUserService<T> : IService<T> where T : User
    {
        T Authenticate(string username, string password);
    }
}
