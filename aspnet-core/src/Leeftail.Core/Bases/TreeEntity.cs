﻿using System.Collections.Generic;
using MongoDB.Bson;
using Leeftail.Core.Interfaces;

namespace Leeftail.Core.Bases
{
    public class TreeEntity<T> : EntityAuditBase, ITreeEntity<T> where T : TreeEntity<T>
    {
        public ObjectId ParentId { get; set; }
        public string AncestorIds { get; set; }
        public bool IsAbstract { get; set; }
        public int Level => AncestorIds?.Split('-').Length ?? 0;
        public T Parent { get; set; }
        public ICollection<T> Children { get; set; }
    }
}
