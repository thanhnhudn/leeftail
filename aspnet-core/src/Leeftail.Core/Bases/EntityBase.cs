﻿using Leeftail.Core.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Leeftail.Core.Bases
{
    public abstract class EntityBase : IEntityBase
    {
        [BsonId]
        public ObjectId Id { get; set; }
    }
}
