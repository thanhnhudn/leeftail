﻿using MongoDB.Bson;
using Leeftail.Core.Interfaces;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Leeftail.Core.Bases
{
    public abstract class EntityAuditBase : IEntityAuditBase
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("Order")]
        public int Order { get; set; }
        [BsonElement("Deleted")]
        public bool Deleted { get; set; }
        [BsonElement("CreatedOn")]
        public DateTime CreatedOn { get; set; }
        [BsonElement("CreatedBy")]
        public string CreatedBy { get; set; }
        [BsonElement("UpdatedOn")]
        public DateTime? UpdatedOn { get; set; }
        [BsonElement("UpdatedBy")]
        public string UpdatedBy { get; set; }
    }
}
