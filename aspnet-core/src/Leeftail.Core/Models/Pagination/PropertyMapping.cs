using System;
using System.Collections.Generic;
using Leeftail.Core.Bases;

namespace Leeftail.Core.Models.Pagination
{
    public abstract class PropertyMapping
    {
        public Dictionary<string, PropertyMappingValue> MappingDictionary { get; }

        protected PropertyMapping(Dictionary<string, PropertyMappingValue> mappingDictionary)
        {
            MappingDictionary = mappingDictionary;
            MappingDictionary[nameof(EntityAuditBase.Id)] = new PropertyMappingValue(new List<string> { nameof(EntityAuditBase.Id) });
            MappingDictionary[nameof(EntityAuditBase.Order)] = new PropertyMappingValue(new List<string> { nameof(EntityAuditBase.Order) });
            MappingDictionary[nameof(EntityAuditBase.Deleted)] = new PropertyMappingValue(new List<string> { nameof(EntityAuditBase.Deleted) });
        }

        public bool ValidMappingExistsFor(string fields)
        {
            if (string.IsNullOrWhiteSpace(fields))
            {
                return true;
            }

            var fieldsAfterSplit = fields.Split(',');
            foreach (var field in fieldsAfterSplit)
            {
                var trimmedField = field.Trim();
                var indexOfFirstSpace = trimmedField.IndexOf(" ", StringComparison.Ordinal);
                var propertyName = indexOfFirstSpace == -1 ?
                    trimmedField : trimmedField.Remove(indexOfFirstSpace);
                if (!MappingDictionary.ContainsKey(propertyName))
                {
                    return false;
                }
            }
            return true;
        }
    }
}