﻿namespace Leeftail.Core.Models.Pagination
{
    public enum PaginationUriType
    {
        CurrentPage,
        PreviousPage,
        NextPage
    }
}
