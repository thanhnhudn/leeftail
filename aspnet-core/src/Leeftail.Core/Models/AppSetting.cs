﻿namespace Leeftail.Core
{
    public class AppSetting
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
        public bool IsSSL { get; set; }
        public string Secret { get; set; }
    }
}
