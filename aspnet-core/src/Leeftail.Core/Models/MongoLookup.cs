﻿namespace Leeftail.Core.Models
{
    public class MongoLookup
    {
        public string CollectionName { get; set; }
        public string LocalField { get; set; }
        public string ForeignField { get; set; }
        public string ResultField { get; set; }
    }
}
