﻿using Leeftail.Core.Bases;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Leeftail.Core.DomainModels
{
    public class Role : EntityAuditBase
    {
        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }
    }
}
