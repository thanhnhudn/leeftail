﻿using Leeftail.Core.Bases;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Leeftail.Core.DomainModels
{
    public class Product : EntityAuditBase
    {
        [BsonElement("Title")]
        public string Title { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("Price")]
        public decimal Price { get; set; }
    }
}
