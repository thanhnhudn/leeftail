﻿using Leeftail.Core.Bases;
using Leeftail.Core.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Leeftail.Core.DomainModels
{
    [BsonIgnoreExtraElements]
    public class User : EntityAuditBase
    {
        [BsonElement("Username")]
        public string Username { get; set; }

        [BsonElement("FirstName")]
        public string FirstName { get; set; }

        [BsonElement("LastName")]
        public string LastName { get; set; }

        [BsonElement("PasswordHash")]
        [JsonIgnore]
        public byte[] PasswordHash { get; set; }

        [BsonElement("PasswordSalt")]
        [JsonIgnore]
        public byte[] PasswordSalt { get; set; }

        [BsonIgnore]
        public string Password { get; set; }

        [BsonElement("RoleIds")]
        public List<MongoId> RoleIds { get; set; }

        [BsonIgnoreIfNull]
        public List<Role> Roles { get; set; }
    }
}
