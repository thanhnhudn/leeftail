﻿namespace Leeftail.Core.Interfaces
{
    public interface ICacheProvider
    {
        void Add(string cacheKey, object value, string region);
        object Get(string cacheKey, string region);
        void RemoveItemsInRegion(string region);
    }
}
