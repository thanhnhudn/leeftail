﻿using Leeftail.Core.Bases;
using Leeftail.Core.Models;
using Leeftail.Core.Models.Pagination;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Leeftail.Core.Interfaces
{
    public interface IRepository<T> where T : EntityAuditBase
    {
        IMongoCollection<T> Collection { get; set; }

        IAggregateFluent<T> Include(Expression<Func<T, bool>> filter = null, params MongoLookup[] lookups);
        Task<T> AddAsync(T model);
        Task<List<T>> GetAllAsync(Expression<Func<T, bool>> filter = null);
        Task<T> GetByIdAsync(ObjectId id);
        Task<ReplaceOneResult> UpdateAsync(ObjectId id, T item);
        Task<DeleteResult> DeleteAsync(Expression<Func<T, bool>> expression);
        Task<DeleteResult> DeleteAllAsync();

        Task<PaginatedList<T>> GetPaginatedAsync(PaginationBase parameters);
        Task<PaginatedList<T>> GetPaginatedAsync(PaginationBase parameters, Expression<Func<T, bool>> filter);
        Task<PaginatedList<T>> GetPaginatedAsync(PaginationBase parameters, Expression<Func<T, bool>> filter, params MongoLookup[] lookups);
    }
}
