﻿using MongoDB.Bson;

namespace Leeftail.Core.Interfaces
{
    public interface IEntityBase
    {
        ObjectId Id { get; set; }
    }
}
