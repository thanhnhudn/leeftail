﻿using MongoDB.Driver;
using System;
using System.Threading;

namespace Leeftail.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        void StartTransaction(TransactionOptions transactionOptions = null);
        void CommitTransaction(CancellationToken cancellationToken = default(CancellationToken));
        void CommitTransactionAsync(CancellationToken cancellationToken = default(CancellationToken));
        //bool Save();
        //bool Save(bool acceptAllChangesOnSuccess);
        //Task<bool> SaveAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken));
        //Task<bool> SaveAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}