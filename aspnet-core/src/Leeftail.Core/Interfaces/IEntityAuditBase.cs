﻿using MongoDB.Bson;

namespace Leeftail.Core.Interfaces
{
    public interface IEntityAuditBase : IOrder, IDeleted, IAudit
    {
        ObjectId Id { get; set; }
    }
}
