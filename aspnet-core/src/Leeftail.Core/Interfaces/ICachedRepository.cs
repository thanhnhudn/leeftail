﻿using Leeftail.Core.Bases;

namespace Leeftail.Core.Interfaces
{
    public interface ICachedRepository<T> : IRepository<T> where T : EntityAuditBase
    {
        void ClearCache();
    }
}
