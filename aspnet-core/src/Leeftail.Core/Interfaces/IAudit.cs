﻿using System;

namespace Leeftail.Core.Interfaces
{
    public interface IAudit
    {
        DateTime CreatedOn { get; set; }
        string CreatedBy { get; set; }
        DateTime? UpdatedOn { get; set; }
        string UpdatedBy { get; set; }
    }
}
