﻿using System.Collections.Generic;
using MongoDB.Bson;
using Leeftail.Core.Bases;

namespace Leeftail.Core.Interfaces
{
    public interface ITreeEntity<T> where T : EntityAuditBase
    {
        ObjectId ParentId { get; set; }
        string AncestorIds { get; set; }
        bool IsAbstract { get; set; }
        int Level { get; }
        T Parent { get; set; }
        ICollection<T> Children { get; set; }
    }
}
