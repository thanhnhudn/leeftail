﻿using Leeftail.Core.Interfaces;
using MongoDB.Driver;
using System.Threading;

namespace Leeftail.Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LeeftailContext _context;
        private IClientSessionHandle _clientSession;

        public UnitOfWork(LeeftailContext context)
        {
            _context = context;
        }

        public void StartTransaction(TransactionOptions transactionOptions = null)
        {
            _clientSession = _context.db.Client.StartSession();
            _clientSession.StartTransaction(transactionOptions);
        }

        public void CommitTransaction(CancellationToken cancellationToken = default(CancellationToken))
        {
            _clientSession.CommitTransaction(cancellationToken);
        }

        public void CommitTransactionAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            while (true)
            {
                try
                {
                    _clientSession.CommitTransaction(cancellationToken);
                    break;
                }
                catch (MongoException exception)
                {
                    // can retry commit
                    if (exception.HasErrorLabel("UnknownTransactionCommitResult"))
                    {
                        //Console.WriteLine("UnknownTransactionCommitResult, retrying commit operation");
                        continue;
                    }
                    else
                    {
                        //Console.WriteLine($"Error during commit: {exception.Message}.");
                        throw;
                    }
                }
            }
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}
