﻿using Leeftail.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Security.Authentication;

namespace Leeftail.Infrastructure.Data
{
    public class LeeftailContext : DbContext
    {
        public IMongoDatabase db { get; }

        public LeeftailContext(IOptions<AppSetting> options)
        {
            try
            {
                MongoClientSettings settings = MongoClientSettings.FromUrl(new MongoUrl(options.Value.ConnectionString));
                if (options.Value.IsSSL)
                {
                    settings.SslSettings = new SslSettings { EnabledSslProtocols = SslProtocols.Tls12 };
                }
                var mongoClient = new MongoClient(settings);
                db = mongoClient.GetDatabase(options.Value.Database);
            }
            catch (Exception ex)
            {
                throw new Exception("Can not access to db server.", ex);
            }
        }
    }
}
