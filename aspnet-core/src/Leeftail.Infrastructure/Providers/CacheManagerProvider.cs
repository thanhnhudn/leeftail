﻿using CacheManager.Core;
using Leeftail.Core.Interfaces;

namespace Leeftail.Infrastructure.Providers
{
    public class CacheManagerProvider : ICacheProvider
    {
        ICacheManager<object> CacheManager { get; set; }

        public CacheManagerProvider()
        {
            CacheManager = CacheFactory.Build("getStartedCache", settings =>
            {
                settings.WithSystemRuntimeCacheHandle("leeftail");
            });
        }

        public void Add(string cacheKey, object value, string region)
        {
            CacheManager.Add(cacheKey, value, region);
        }

        public object Get(string cacheKey, string region)
        {
            return CacheManager.Get(cacheKey, region);
        }

        public void RemoveItemsInRegion(string region)
        {
            CacheManager.ClearRegion(region);
        }
    }
}
