﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using System;

namespace Leeftail.Infrastructure.Interfaces
{
    public interface ICoreService<out T> : IDisposable
    {
        ILogger<T> Logger { get; }
        IMapper Mapper { get; }
    }
}