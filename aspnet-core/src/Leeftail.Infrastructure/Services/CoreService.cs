﻿using AutoMapper;
using Leeftail.Infrastructure.Interfaces;
using Microsoft.Extensions.Logging;

namespace Leeftail.Infrastructure.Services
{
    public class CoreService<T> : ICoreService<T>
    {
        public ILogger<T> Logger { get; }
        public IMapper Mapper { get; }

        public CoreService(
            ILogger<T> logger,
            IMapper mapper)
        {
            Logger = logger;
            Mapper = mapper;
        }

        public virtual void Dispose()
        {

        }
    }
}
