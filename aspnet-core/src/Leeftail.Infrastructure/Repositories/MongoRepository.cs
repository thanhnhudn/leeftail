﻿using Leeftail.Core.Bases;
using Leeftail.Core.Interfaces;
using Leeftail.Core.Models;
using Leeftail.Core.Models.Pagination;
using Leeftail.Infrastructure.Data;
using Leeftail.Infrastructure.Extensions;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Leeftail.Infrastructure.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : EntityAuditBase
    {
        public IMongoCollection<T> Collection { get; set; }

        public MongoRepository(LeeftailContext context)
        {
            // Check collection is existing, else create new
            string collectionName = typeof(T).Name;
            var filter = new BsonDocument("name", collectionName);
            var options = new ListCollectionNamesOptions { Filter = filter };
            bool collectionExist = context.db.ListCollectionNames(options).Any();
            if (!collectionExist) context.db.CreateCollection(collectionName);
            Collection = context.db.GetCollection<T>(collectionName);
        }

        public IAggregateFluent<T> Include(Expression<Func<T, bool>> filter = null, params MongoLookup[] lookups)
        {
            return Collection.Include(filter, lookups);
        }

        public async Task<T> AddAsync(T model)
        {
            model.CreatedOn = DateTime.Now;
            await Collection.InsertOneAsync(model);
            return model;
        }

        public virtual async Task<List<T>> GetAllAsync(Expression<Func<T, bool>> filter = null)
        {
            return await Collection.Find(filter ?? (x => true)).ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(ObjectId id)
        {
            return await Collection.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<ReplaceOneResult> UpdateAsync(ObjectId id, T item)
        {
            item.Id = id;
            item.UpdatedOn = DateTime.Now;
            return await Collection.ReplaceOneAsync(r => r.Id == id, item);
        }

        public async Task<DeleteResult> DeleteAsync(Expression<Func<T, bool>> expression)
        {
            return await Collection.DeleteOneAsync(expression);
        }

        public async Task<DeleteResult> DeleteAllAsync()
        {
            return await Collection.DeleteManyAsync(new BsonDocument());
        }

        public virtual async Task<PaginatedList<T>> GetPaginatedAsync(PaginationBase parameters)
        {
            var collectionBeforePaging = Collection.Find(FilterDefinition<T>.Empty).Sort(parameters.OrderByString);
            parameters.Count = await collectionBeforePaging.CountDocumentsAsync();
            var items = await collectionBeforePaging.Skip(parameters.PageIndex * parameters.PageSize).Limit(parameters.PageSize).ToListAsync();
            var result = new PaginatedList<T>(parameters, items);
            return result;
        }

        public virtual async Task<PaginatedList<T>> GetPaginatedAsync(PaginationBase parameters, Expression<Func<T, bool>> filter)
        {
            var collectionBeforePaging = Collection.Find(filter).Sort(parameters.OrderByString);
            parameters.Count = await collectionBeforePaging.CountDocumentsAsync();
            var items = await collectionBeforePaging.Skip(parameters.PageIndex * parameters.PageSize).Limit(parameters.PageSize).ToListAsync();
            var result = new PaginatedList<T>(parameters, items);
            return result;
        }

        public virtual async Task<PaginatedList<T>> GetPaginatedAsync(PaginationBase parameters, Expression<Func<T, bool>> filter, params MongoLookup[] lookups)
        {
            // Lookup method doesn't support on count document
            var collectionBeforePaging = Collection.Find(filter).Sort(parameters.OrderByString);
            parameters.Count = await collectionBeforePaging.CountDocumentsAsync();

            var items = await Collection.Include(filter, lookups).Sort(parameters.OrderByString)
                .Skip(parameters.PageIndex * parameters.PageSize).Limit(parameters.PageSize).ToListAsync();
            var result = new PaginatedList<T>(parameters, items);
            return result;
        }
    }
}
