﻿using Leeftail.Core.Bases;
using Leeftail.Core.Interfaces;
using Leeftail.Infrastructure.Data;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Leeftail.Infrastructure.Repositories
{
    public class MongoCachedRepository<T> : MongoRepository<T>, ICachedRepository<T> where T : EntityAuditBase
    {
        protected readonly ICacheProvider CacheProvider;
        protected readonly string Region = typeof(T).Name;

        public MongoCachedRepository(LeeftailContext context, ICacheProvider cacheProvider)
            : base(context)
        {
            CacheProvider = cacheProvider;
        }

        public override async Task<List<T>> GetAllAsync(Expression<Func<T, bool>> filter = null)
        {
            var cacheKey = Region + "-GetList-" + filter ?? filter.ToString();
            var entities = CacheProvider.Get(cacheKey, Region) as List<T>;
            if (entities != null)
                return entities;
            entities = await base.GetAllAsync(filter);
            CacheProvider.Add(cacheKey, entities, Region);
            return entities;
        }

        public override async Task<T> GetByIdAsync(ObjectId id)
        {
            var cacheKey = Region + "-Get-" + id.ToString();
            var entity = CacheProvider.Get(cacheKey, Region) as T;
            if (entity != null)
                return entity;
            entity = await base.GetByIdAsync(id);
            CacheProvider.Add(cacheKey, entity, Region);
            return entity;
        }

        public void ClearCache()
        {
            CacheProvider.RemoveItemsInRegion(Region);
        }
    }
}
