﻿using Leeftail.Core.Models;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;

namespace Leeftail.Infrastructure.Extensions
{
    public static class MongoExtensions
    {
        public static IAggregateFluent<T> Include<T>(this IMongoCollection<T> source, Expression<Func<T, bool>> filter = null, params MongoLookup[] lookups)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            var aggregatedList = source.Aggregate().Match(filter ?? (x => true));
            foreach (var item in lookups)
            {
                aggregatedList = aggregatedList.Lookup<dynamic, T>(item.CollectionName, item.LocalField, item.ForeignField, item.ResultField);
            }

            return aggregatedList;
        }
    }
}
