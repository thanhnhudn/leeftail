﻿namespace Leeftail.Api.Constants
{
    public class Constants
    {
        public struct Roles
        {
            public const string Admin = "Admin";
            public const string User = "User";
        }
    }
}
