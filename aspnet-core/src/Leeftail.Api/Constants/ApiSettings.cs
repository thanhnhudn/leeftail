﻿namespace Leeftail.Api.Constants
{
    public class LeeftailApiSettings
    {
        public const string ApiName = "leeftailapi";
        public const string ApiDisplayName = "Leeftail API";

        public const string CorsPolicyName = "leeftail";
        public const string ClientId = "leeftail";
        public const string ClientName = "Leeftail";
    }
}
