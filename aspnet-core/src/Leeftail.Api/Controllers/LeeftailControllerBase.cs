﻿using AutoMapper;
using Leeftail.Api.Helpers;
using Leeftail.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using System;

namespace Leeftail.Api.Controllers
{
    public abstract class LeeftailControllerBase<T> : Controller
    {
        protected readonly ILogger<T> Logger;
        protected readonly IMapper Mapper;
        protected readonly ICoreService<T> CoreService;

        protected LeeftailControllerBase(ICoreService<T> coreService)
        {
            CoreService = coreService;
            Logger = coreService.Logger;
            Mapper = coreService.Mapper;
        }

        #region Current Information

        protected DateTimeOffset Now => DateTime.Now;
        protected string UserName => User.Identity.Name ?? "Anonymous";
        protected DateTimeOffset Today => DateTime.Now.Date;

        #endregion

        #region HTTP Status Codes

        protected UnprocessableEntityObjectResult UnprocessableEntity(ModelStateDictionary modelState)
        {
            return new UnprocessableEntityObjectResult(modelState);
        }

        #endregion
    }
}