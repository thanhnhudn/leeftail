﻿using Leeftail.Api.ViewModels;
using Leeftail.Api.ViewModels.Common;
using Leeftail.Api.ViewModels.Hateoas;
using Leeftail.Core.DomainModels;
using Leeftail.Core.Models.Pagination;
using Leeftail.Infrastructure.Extensions;
using Leeftail.Infrastructure.Interfaces;
using Leeftail.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leeftail.Api.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class ProductController : LeeftailControllerBase<ProductController>
    {
        private readonly IService<Product> _productService;
        private readonly IUrlHelper _urlHelper;

        public ProductController(
            ICoreService<ProductController> coreService,
            IService<Product> productService,
            IUrlHelper urlHelper) : base(coreService)
        {
            _productService = productService;
            _urlHelper = urlHelper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var items = await _productService.GetAllAsync();
            var results = Mapper.Map<IEnumerable<ProductViewModel>>(items);
            return Ok(results.OrderBy(r => r.CreatedOn));
        }

        [HttpGet]
        [Route("Paged", Name = "GetPagedProducts")]
        public async Task<IActionResult> GetPaged(QueryViewModel parameters)
        {
            PaginatedList<Product> pagedList;
            if (string.IsNullOrEmpty(parameters.SearchTerm))
            {
                pagedList = await _productService.GetPaginatedAsync(parameters);
            }
            else
            {
                pagedList = await _productService.GetPaginatedAsync(parameters,
                    x => x.Title.Contains(parameters.SearchTerm) || x.Description.Contains(parameters.SearchTerm));
            }
            var productVms = Mapper.Map<IEnumerable<ProductViewModel>>(pagedList);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(pagedList.PaginationBase, new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            }));

            //var links = CreateLinksForProducts(parameters, pagedList.HasPrevious, pagedList.HasNext);
            //var dynamicProducts = productVms.ToDynamicIEnumerable(parameters.Fields);
            //var dynamicProductsWithLinks = dynamicProducts.Select(product =>
            //{
            //    var productDictionary = product as IDictionary<string, object>;
            //    var productLinks = CreateLinksForProduct((int)productDictionary["Id"], parameters.Fields);
            //    productDictionary.Add("links", productLinks);
            //    return productDictionary;
            //});
            //var resultWithLinks = new
            //{
            //    Links = links,
            //    Value = dynamicProductsWithLinks
            //};
            //return Ok(resultWithLinks);
            return Ok(pagedList);
        }

        [HttpGet]
        [Route("{id}", Name = "GetProduct")]
        public async Task<IActionResult> Get(string id, string fields)
        {
            var objectId = new ObjectId(id);
            var item = await _productService.GetByIdAsync(objectId);
            if (item == null)
            {
                return NotFound();
            }
            var productVm = Mapper.Map<ProductViewModel>(item);
            var links = CreateLinksForProduct(objectId, fields);
            var dynamicObject = productVm.ToDynamic(fields) as IDictionary<string, object>;
            dynamicObject.Add("links", links);
            return Ok(dynamicObject);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductCreateViewModel productVm)
        {
            if (productVm == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            var newItem = Mapper.Map<Product>(productVm);
            newItem.CreatedBy = User.Identity.Name;
            await _productService.AddAsync(newItem);

            var vm = Mapper.Map<ProductViewModel>(newItem);

            //var links = CreateLinksForProduct(vm.Id);
            var dynamicObject = productVm.ToDynamic() as IDictionary<string, object>;
            //dynamicObject.Add("links", links);

            //return CreatedAtRoute("GetProduct", new { id = dynamicObject["id"] }, dynamicObject);
            return Ok();
        }

        [HttpPut("{id}", Name = "UpdateProduct")]
        public async Task<IActionResult> Put(string id, [FromBody] ProductEditViewModel productVm)
        {
            if (productVm == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(ModelState);
            }

            var objectId = new ObjectId(id);
            var dbItem = await _productService.GetByIdAsync(objectId);
            if (dbItem == null)
            {
                return NotFound();
            }

            Mapper.Map(productVm, dbItem);
            dbItem.UpdatedBy = User.Identity.Name;
            await _productService.UpdateAsync(objectId, dbItem);

            return NoContent();
        }

        [HttpDelete("{id}", Name = "DeleteProduct")]
        public async Task<IActionResult> Delete(string id)
        {
            ObjectId objectId = new ObjectId(id);
            var model = await _productService.GetByIdAsync(objectId);
            if (model == null)
            {
                return NotFound();
            }
            await _productService.DeleteAsync(p => p.Id == model.Id);

            return NoContent();
        }

        private string CreateProductUri(PaginationBase parameters, PaginationUriType uriType)
        {
            switch (uriType)
            {
                case PaginationUriType.PreviousPage:
                    var previousParameters = parameters.Clone();
                    previousParameters.PageIndex--;
                    return _urlHelper.Link("GetPagedProducts", previousParameters);
                case PaginationUriType.NextPage:
                    var nextParameters = parameters.Clone();
                    nextParameters.PageIndex++;
                    return _urlHelper.Link("GetPagedProducts", nextParameters);
                case PaginationUriType.CurrentPage:
                default:
                    return _urlHelper.Link("GetPagedProducts", parameters);
            }
        }

        private IEnumerable<LinkViewModel> CreateLinksForProduct(ObjectId id, string fields = null)
        {
            var links = new List<LinkViewModel>
            {
                string.IsNullOrWhiteSpace(fields)
                    ? new LinkViewModel(_urlHelper.Link("GetProduct", new {id}), "self", "GET")
                    : new LinkViewModel(_urlHelper.Link("GetProduct", new {id, fields}), "self", "GET"),
                new LinkViewModel(_urlHelper.Link("UpdateProduct", new {id}), "update_product", "PUT"),
                new LinkViewModel(_urlHelper.Link("PartiallyUpdateProduct", new {id}), "partially_update_product", "PATCH"),
                new LinkViewModel(_urlHelper.Link("DeleteProduct", new {id}), "delete_product", "DELETE")
            };
            return links;
        }

        private IEnumerable<LinkViewModel> CreateLinksForProducts(PaginationBase parameters, bool hasPrevious, bool hasNext)
        {
            var links = new List<LinkViewModel>
            {
                new LinkViewModel(CreateProductUri(parameters, PaginationUriType.CurrentPage), "self", "GET")
            };
            if (hasPrevious)
            {
                links.Add(new LinkViewModel(CreateProductUri(parameters, PaginationUriType.PreviousPage), "previous_page", "GET"));
            }
            if (hasNext)
            {
                links.Add(new LinkViewModel(CreateProductUri(parameters, PaginationUriType.NextPage), "next_page", "GET"));
            }
            return links;
        }
    }
}
