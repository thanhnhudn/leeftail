﻿using Leeftail.Api.ViewModels;
using Leeftail.Api.ViewModels.Common;
using Leeftail.Core;
using Leeftail.Core.DomainModels;
using Leeftail.Core.Models.Pagination;
using Leeftail.Infrastructure.Interfaces;
using Leeftail.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Leeftail.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UserController : LeeftailControllerBase<UserController>
    {
        private readonly IUserService<User> _userService;
        private readonly AppSetting _appSetting;

        public UserController(
            ICoreService<UserController> coreService,
            IUserService<User> userService,
            IOptions<AppSetting> options) : base(coreService)
        {
            _userService = userService;
            _appSetting = options.Value;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]UserCreateViewModel userDto)
        {
            var user = _userService.Authenticate(userDto.Username, userDto.Password);

            if (user == null)
                return BadRequest("Username or password is incorrect");

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSetting.Secret);
            var claims = new List<Claim>() { new Claim(ClaimTypes.Name, user.Username) };
            foreach (var role in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Name));
            }
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info (without password) and token to store client side
            return new OkObjectResult(new
            {
                Id = user.Id,
                Username = user.Username,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Token = tokenString
            });
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]UserCreateViewModel user)
        {
            try
            {
                var mapUser = Mapper.Map<User>(user);
                var model = await _userService.AddAsync(mapUser);
                var results = Mapper.Map<UserViewModel>(model);
                return Ok(results);
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var items = await _userService.GetAllAsync();
            var results = Mapper.Map<IEnumerable<UserViewModel>>(items);
            return Ok(results);
        }

        [HttpGet]
        [Route("Paged")]
        public async Task<IActionResult> GetPaged(QueryViewModel parameters)
        {
            PaginatedList<User> pagedList;
            if (string.IsNullOrEmpty(parameters.SearchTerm))
            {
                pagedList = await _userService.GetPaginatedAsync(parameters);
            }
            else
            {
                pagedList = await _userService.GetPaginatedIncludeAsync(parameters,
                    x => x.FirstName.Contains(parameters.SearchTerm) || x.LastName.Contains(parameters.SearchTerm));
            }
            var productVms = Mapper.Map<IEnumerable<UserViewModel>>(pagedList);
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(pagedList.PaginationBase, new JsonSerializerSettings
            {
                ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver()
            }));

            return Ok(pagedList);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var objectId = new ObjectId(id);
            var item = await _userService.GetByIdAsync(objectId);
            if (item == null)
            {
                return NotFound();
            }
            var results = Mapper.Map<UserViewModel>(item);
            return Ok(results);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(string id, [FromBody]UserEditViewModel user)
        {
            try
            {
                var mapUser = Mapper.Map<User>(user);
                await _userService.UpdateAsync(new ObjectId(id), mapUser);
                return Ok();
            }
            catch (Exception ex)
            {
                // return error message if there was an exception
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = Constants.Constants.Roles.Admin)]
        public async Task<IActionResult> Delete(string id)
        {
            ObjectId objectId = new ObjectId(id);
            var model = await _userService.GetByIdAsync(objectId);
            if (model == null)
            {
                return NotFound();
            }
            await _userService.DeleteAsync(p => p.Id == model.Id);
            return NoContent();
        }
    }
}
