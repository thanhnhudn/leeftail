﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Leeftail.Api.Configurations;
using Leeftail.Infrastructure.Data;
using Serilog;

namespace Leeftail.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "Leeftail API";
            var host = BuildWebHost(args);
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var loggerFactory = services.GetRequiredService<ILoggerFactory>();
                try
                {
                    //var salesContext = services.GetRequiredService<LeeftailContext>();
                    //SalesContextSeed.SeedAsync(salesContext, loggerFactory).Wait();
                }
                catch (Exception ex)
                {
                    var logger = loggerFactory.CreateLogger<Program>();
                    logger.LogError(ex, "An error occurred seeding the DB.");
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseIISIntegration()
                .UseUrls(Environment.GetEnvironmentVariable("LeeftailApi:ServerBase"))
                .UseStartup<Startup>()
                .UseSerilog()
                .Build();
    }
}
