﻿using FluentValidation;

namespace Leeftail.Api.ViewModels.Validators
{
    public class ProductCreateValidator : AbstractValidator<ProductCreateViewModel>
    {
        public ProductCreateValidator()
        {
            RuleFor(p => p.Title).NotEmpty().MaximumLength(10).WithName("Product Name")
                .WithMessage("Please specify a {PropertyName}, And the length should be less than {MaximumLength}");
            RuleFor(p => p.Description).MaximumLength(100);
            //RuleFor(p => p.EquivalentTon).GreaterThan(0).WithMessage("{PropertyName} should greater than {GreaterThan}");
            //RuleFor(p => p.TaxRate).GreaterThanOrEqualTo(0).WithMessage("{PropertyName} should greater than or equal to {GreaterThan}");
        }
    }
}