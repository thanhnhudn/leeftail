﻿using System.Collections.Generic;

namespace Leeftail.Api.ViewModels
{
    public class UserCreateViewModel
    {
        public UserCreateViewModel()
        {
            Roles = new List<RoleViewModel>() { new RoleViewModel() { Name = Constants.Constants.Roles.User } };
        }

        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public List<RoleViewModel> Roles { get; set; }
    }
}
