﻿using Leeftail.Core.Bases;
using System.Collections.Generic;

namespace Leeftail.Api.ViewModels
{
    public class UserViewModel : EntityAuditBase
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<RoleViewModel> Roles { get; set; }
    }
}
