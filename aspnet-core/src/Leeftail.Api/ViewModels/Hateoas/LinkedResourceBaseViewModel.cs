using System.Collections.Generic;
using Leeftail.Core.Bases;

namespace Leeftail.Api.ViewModels.Hateoas
{
    public abstract class LinkedResourceBaseViewModel: EntityAuditBase
    {
        public List<LinkViewModel> Links { get; set; } = new List<LinkViewModel>();
    }
}