﻿using Leeftail.Core.Models.Pagination;
using System;
using System.Collections.Generic;

namespace Leeftail.Api.ViewModels.PropertyMappings
{
    public class ProductPropertyMapping : PropertyMapping
    {
        public ProductPropertyMapping() : base(
            new Dictionary<string, PropertyMappingValue>(StringComparer.OrdinalIgnoreCase)
                {
                    //{ nameof(ProductViewModel.Title), new PropertyMappingValue(new [] { nameof(Product.Name) } )},
                    //{ nameof(ProductViewModel.Price), new PropertyMappingValue(new [] { nameof(Product.EquivalentTon) } )},
                    //{ nameof(ProductViewModel.ShelfLife), new PropertyMappingValue(new [] { nameof(Product.ShelfLife) } )},
                    //{ nameof(ProductViewModel.TaxRate), new PropertyMappingValue(new [] { nameof(Product.TaxRate) } )},
                })
        {
        }
    }
}
