﻿using Leeftail.Core.Models.Pagination;

namespace Leeftail.Api.ViewModels.Common
{
    public class QueryViewModel : PaginationBase
    {
        public string SearchTerm { get; set; }
        public string Fields { get; set; }
    }
}
