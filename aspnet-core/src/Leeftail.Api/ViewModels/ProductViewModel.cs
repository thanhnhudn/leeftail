﻿using Leeftail.Core.Bases;
using Leeftail.Core.DomainModels.Enums;

namespace Leeftail.Api.ViewModels
{
    public class ProductViewModel: EntityAuditBase
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
