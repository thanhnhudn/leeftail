﻿using Leeftail.Core.Bases;

namespace Leeftail.Api.ViewModels
{
    public class RoleViewModel : EntityAuditBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
