﻿using Leeftail.Core.Interfaces;

namespace Leeftail.Api.ViewModels
{
    public class ProductCreateViewModel : IOrder
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Order { get; set; }
    }
}
