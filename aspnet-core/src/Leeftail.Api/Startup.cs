﻿using AutoMapper;
using Leeftail.Api.Configurations;
using Leeftail.Api.Constants;
using Leeftail.Core;
using Leeftail.Infrastructure.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System.Text;

namespace Leeftail.Api
{
    public class Startup
    {
        private IServiceCollection _services;
        public static IConfiguration Configuration { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<SalesContext>(options => options.UseSqlServer(Configuration["SalesApi:DefaultConnection"]));
            services.AddDbContext<LeeftailContext>();
            services.AddMvc(options =>
            {
                options.ReturnHttpNotAcceptable = true;
                // set authorization on all controllers or routes
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.Converters.Add(new ObjectIdConverter());
                })
                .AddFluetValidations();

            services.Configure<AppSetting>(options =>
            {
                options.ConnectionString = Configuration["MongoConnection:ConnectionString"];
                options.Database = Configuration["MongoConnection:Database"];
                options.Secret = Configuration["Secret:Key"];
            });

            services.AddAutoMapper();
            services.AddServices();
            services.AddRepositories();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = LeeftailApiSettings.ApiDisplayName, Version = "v1" });
            });

            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();

            //services.AddAuthentication("Bearer")
            //    .AddIdentityServerAuthentication(options =>
            //    {
            //        options.Authority = Configuration["AuthorizationServer:ServerBase"];
            //        options.RequireHttpsMetadata = false;
            //        options.ApiName = LeeftailApiSettings.ApiName;
            //    });

            // configure jwt authentication
            var key = Encoding.ASCII.GetBytes(Configuration["Secret:Key"]);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            services.AddCors(options =>
            {
                options.AddPolicy(LeeftailApiSettings.CorsPolicyName, policy =>
                {
                    policy.WithOrigins(Configuration["LeeftailApi:ClientBase"])
                        .AllowAnyHeader()
                        .AllowAnyOrigin()
                        .AllowAnyMethod();
                });
            });

            _services = services;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                ListAllRegisteredServices(app);
            }
            else
            {
                app.InitializeDatabase();
            }
            app.UseExceptionHandlingMiddleware();
            app.UseCors(LeeftailApiSettings.CorsPolicyName);
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", LeeftailApiSettings.ClientName + " API v1");
            });
            app.UseAuthentication();
            app.UseMvc();
        }

        private void ListAllRegisteredServices(IApplicationBuilder app)
        {
            app.Map("/allservices", builder => builder.Run(async context =>
            {
                var sb = new StringBuilder();
                sb.Append("<h1>All Services</h1>");
                sb.Append("<table><thead>");
                sb.Append("<tr><th>Type</th><th>Lifetime</th><th>Instance</th></tr>");
                sb.Append("</thead><tbody>");
                foreach (var svc in _services)
                {
                    sb.Append("<tr>");
                    sb.Append($"<td>{svc.ServiceType.FullName}</td>");
                    sb.Append($"<td>{svc.Lifetime}</td>");
                    sb.Append($"<td>{svc.ImplementationType?.FullName}</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
                await context.Response.WriteAsync(sb.ToString());
            }));
        }
    }
}
