﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Leeftail.Infrastructure.Data;

namespace Leeftail.Api.Configurations
{
    public static class DatabaseExtensions
    {
        public static void InitializeDatabase(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                //var leeftailContext = serviceScope.ServiceProvider.GetRequiredService<LeeftailContext>();
                //leeftailContext.Database.Migrate();
            }
        }
    }
}
