﻿using AutoMapper;
using Leeftail.Api.ViewModels;
using Leeftail.Core.DomainModels;

namespace Leeftail.Api.Configurations
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName => "ViewModelToDomainMappings";

        public ViewModelToDomainMappingProfile()
        {
            CreateMap<UserViewModel, User>();
            CreateMap<UserCreateViewModel, User>();
            CreateMap<UserEditViewModel, User>();

            CreateMap<ProductViewModel, Product>();
            CreateMap<ProductCreateViewModel, Product>();
            CreateMap<ProductEditViewModel, Product>();
        }
    }
}