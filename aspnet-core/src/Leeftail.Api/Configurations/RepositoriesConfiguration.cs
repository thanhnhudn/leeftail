﻿using Leeftail.Core.DomainModels;
using Leeftail.Core.Interfaces;
using Leeftail.Infrastructure.Repositories;
using Leeftail.Service.Interfaces;
using Leeftail.Service.Repositories;
using Leeftail.Service.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Leeftail.Api.Configurations
{
    public static class RepositoriesConfiguration
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(MongoRepository<>));
            services.AddScoped(typeof(ICachedRepository<>), typeof(MongoCachedRepository<>));
            services.AddScoped(typeof(IUserService<User>), typeof(UserService));
            services.AddScoped(typeof(IService<Product>), typeof(ProductService));
        }
    }
}
