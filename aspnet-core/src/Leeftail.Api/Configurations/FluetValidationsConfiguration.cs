﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using Leeftail.Api.ViewModels;
using Leeftail.Api.ViewModels.Validators;

namespace Leeftail.Api.Configurations
{
    public static class FluetValidationsConfiguration
    {
        public static void AddFluetValidations(this IMvcBuilder mvcBuilder)
        {
            mvcBuilder.AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<ProductCreateValidator>());
        }
    }
}
