﻿using AutoMapper;
using Leeftail.Api.ViewModels;
using Leeftail.Core.DomainModels;

namespace Leeftail.Api.Configurations
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName => "DomainToViewModelMappings";

        public DomainToViewModelMappingProfile()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<Role, RoleViewModel>();
            CreateMap<Product, ProductViewModel>();
        }
    }
}